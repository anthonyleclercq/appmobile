


import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mspr/main.dart';

void main() {
  testWidgets('Test le widget de la liste des réductions', (WidgetTester tester) async {
    await tester.pumpWidget(MyHomePage());

    final titleFinder = find.text('Liste des réductions');
    final nameCodeFinder = find.text('toto20');

    expect(titleFinder, findsOneWidget);
    expect(nameCodeFinder, findsOneWidget);
  });

  testWidgets('Test le widget de l\'affichage d\'une réductions', (WidgetTester tester) async {
    await tester.pumpWidget(DetailCode());

    final titleFinder = find.text('toto20');
    final dateCodeFinder = find.text('Date de début de la promo : ');

    expect(titleFinder, findsOneWidget);
    expect(dateCodeFinder, findsOneWidget);
  });
}