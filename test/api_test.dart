// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mspr/Code.dart';

class ApiConnection {
  String urlProd = 'https://inspired-shell-294508.ew.r.appspot.com';
  String urlTest = 'http://127.0.0.1:5000';
  Future<int> testgetCodes() async {
    final response =
        await http.get( urlTest + '/api/reduction/all');
    return response.statusCode;
  }

  Future<Code> testGetCodeByID(int id) async {
    final response = await http.get(
        urlTest + '/api/reduction/' + id.toString());
    return Code.fromJson(jsonDecode(response.body));
  }

  Future<Code> testgetFirstCodes() async {
    final response =
    await http.get(urlTest + '/api/reduction/all');
    var testConvert = jsonDecode(response.body) as List;
    List<Code> test =  testConvert.map((e) => Code.fromJson(e)).toList();
    return test.first;
  }

  Future<Code> testGetCodeByCode(String code) async {
    final response = await http
        .get(urlTest + '/api/reduction/code/' + code);
    return Code.fromJson(jsonDecode(response.body));
  }
}

void main() {
  final apiConnection = ApiConnection();
  Code code = Code(
      id: 1,
      qRcode: '20%',
      code: 'toto20',
      description: '-20% sur toutes la gamme cp',
      dateDebut: '01/01/2020',
      dateFin: '02/02/2020');

  test('Test Get All Promos', () async {
    int responseCode = await apiConnection.testgetCodes();
    print("Statut de la requête : " + responseCode.toString());
    expect(responseCode, 200);
  });

  test('Test Get First Promos', () async {
    Code responseFirstCode = await apiConnection.testgetFirstCodes();
    print("Statut de la requête : " + responseFirstCode.toString());
    expect(responseFirstCode.toString(), code.toString());
  });

  test('Test Get Code By ID', () async {
    Code codeTest = await apiConnection.testGetCodeByID(1);
    print(codeTest.toString());
    expect(codeTest.toString(), code.toString());
  });

  test('Test Get Code By CodePromo', () async {
    Code codeTest = await apiConnection.testGetCodeByCode("toto20");
    print(codeTest.toString());
    expect(codeTest.toString(), code.toString());
  });
}
