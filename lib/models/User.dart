class User {
  int id;
  String login;
  String password;

  User(id, login, password){
    this.id = id;
    this.login = login;
    this.password = password;
  }

  User.fromJson(Map json):
    id = json['id'],
    login = json['login'],
    password = json['password'];

  Map toJson(){
    return {'id': id, 'username': login, 'password': password};
  }

}